# Proyecto Final Microelectrónica

* Gabriel Gutiérrez Arguedas B63215
________________________________________________
________________________________________________

# Instrucciones de uso:
A continuación se muestran los comandos que debe ejecutar para ver los resultadosde la descripción conductual de la parte A y B, así como las pruebas realizadas. Asegúrese de ejecutarlos dentro del directorio /Parte_A_y_B

#### Compilar el programa
    $ make iverilog
    
#### Motor del tiempo de ejecución
    $ make vvp

#### Visualizar las formas de onda
    $ make gtk