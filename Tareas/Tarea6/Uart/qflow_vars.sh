#!/usr/bin/tcsh -f
#-------------------------------------------
# qflow variables for project /home/gabriel/Desktop/Microelectronica/Tareas/Tarea6/Uart
#-------------------------------------------

set projectpath=/home/gabriel/Desktop/Microelectronica/Tareas/Tarea6/Uart
set techdir=/usr/share/qflow/tech/osu050
set sourcedir=/home/gabriel/Desktop/Microelectronica/Tareas/Tarea6/Uart/source
set synthdir=/home/gabriel/Desktop/Microelectronica/Tareas/Tarea6/Uart/synthesis
set layoutdir=/home/gabriel/Desktop/Microelectronica/Tareas/Tarea6/Uart/layout
set techname=osu050
set scriptdir=/usr/lib/qflow/scripts
set bindir=/usr/lib/qflow/bin
set synthlog=/home/gabriel/Desktop/Microelectronica/Tareas/Tarea6/Uart/synth.log
#-------------------------------------------

