#!/usr/bin/tcsh -f
#-------------------------------------------
# qflow variables for project /home/gabriel/Desktop/Microelectronica/Tareas/Arbiter
#-------------------------------------------

set projectpath=/home/gabriel/Desktop/Microelectronica/Tareas/Arbiter
set techdir=/usr/share/qflow/tech/osu050
set sourcedir=/home/gabriel/Desktop/Microelectronica/Tareas/Arbiter/source
set synthdir=/home/gabriel/Desktop/Microelectronica/Tareas/Arbiter/synthesis
set layoutdir=/home/gabriel/Desktop/Microelectronica/Tareas/Arbiter/layout
set techname=osu050
set scriptdir=/usr/lib/qflow/scripts
set bindir=/usr/lib/qflow/bin
set synthlog=/home/gabriel/Desktop/Microelectronica/Tareas/Arbiter/synth.log
#-------------------------------------------

