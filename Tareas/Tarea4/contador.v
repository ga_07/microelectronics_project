`timescale 1ns / 1ps

module contador #(
    parameter S0 = 2'b00, //Caso modo 00
    parameter S1 = 2'b01, //Caso modo 01
    parameter S2 = 2'b10, //Caso modo 10
    parameter S3 = 2'b11  //Caso modo 11
)(
    input clk, en,
    input [1:0] Mode,
    input [3:0] D,
    output reg [3:0] Q,
    output reg rco
); 

    always @(posedge clk)begin
        if (!en) begin
            Q <= D;
            rco <= 1;
        end else begin
            case(Mode)
                S0: begin
                    Q <= Q+1;
                    if (Q == 4'hF) begin
                        rco <= 1;
                    end else begin
                        rco <= 0;
                    end
                end
                S1: begin
                    Q <= Q-1;
                    if (Q == 4'hF) begin
                        rco <= 1;
                    end else begin
                        rco <= 0;
                    end
                end
                S2: begin
                    Q <= Q-3;
                    if (Q == 4'hF | Q == 4'h0 | Q == 4'h1) begin
                        rco <= 1;
                    end else begin
                        rco <= 0;
                    end
                end
                S3: begin
                    Q <= D;
                    rco <= 1;
                end
                default: begin
                    Q <= D;
                    rco <= 1;
                end
            endcase
        end
    end

endmodule