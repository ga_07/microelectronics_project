task driver_init;
input integer iteration;
  begin
    @(negedge clk)
      en = 0;
      D = 4'b0000;
      Mode = 2'b00;
    @(negedge clk)
    @(negedge clk)
      en = 0;
      D = 4'b1010;
      Mode = 2'b00;
    @(negedge clk)
    @(negedge clk)
      en = 0;
      D = 4'b1011;
      Mode = 2'b00;
    @(negedge clk)
    @(negedge clk)
      en = 0;
      D = 4'b0111;
      Mode = 2'b00;
    @(negedge clk)
    @(negedge clk)
      en = 0;
      D = 4'b1001;
      Mode = 2'b00;
    @(negedge clk)
    @(negedge clk)
      en = 0;
      D = 4'b0110;
      Mode = 2'b00;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 4'b0100;
      Mode = 2'b11;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 4'b1111;
      Mode = 2'b11;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 4'b0001;
      Mode = 2'b11;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 4'b1011;
      Mode = 2'b11;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 4'b1000;
      Mode = 2'b11;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 4'b0101;
      Mode = 2'b11;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 4'b0000;
      Mode = 2'b11;
    @(negedge clk)
    @(negedge clk)
    repeat (iteration) begin
        @(negedge clk) begin
            en = 1;
            Mode = 2'b00; 
        end
    end
    @(negedge clk)
      en = 1;
      D = 4'b1111;
      Mode = 2'b11;
    @(negedge clk)
    repeat (iteration) begin
        @(negedge clk) begin
            en = 1;
            Mode = 2'b01; 
        end
    end
    @(negedge clk)
      en = 1;
      D = 4'b1111;
      Mode = 2'b11;
    @(negedge clk)
    repeat (iteration) begin
        @(negedge clk) begin
            en = 1;
            Mode = 2'b10; 
        end
    end      
  end
endtask

task driver_request;
input integer iteration;

  repeat (iteration) begin  
    @(negedge clk) begin
        Mode = $random;
        D = $random;
    end
  end
endtask