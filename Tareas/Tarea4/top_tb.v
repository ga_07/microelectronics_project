`timescale 1ns / 1ps
`include "contador.v"
`include "contador_tb.v"
`include "contador_synt.v"

module top_tb;

    wire clk, en, rco, rco_estr, rco_sb;
    wire [1:0] Mode;
    wire [3:0] D, Q, Q_estr, Q_sb;

    contador conductual(
        .clk(clk), 
        .en(en),
        .Mode(Mode),
        .D(D),
        .Q(Q),
        .rco(rco)
    );

    contador_tb testbench(
        .clk(clk), 
        .en(en),
        .Mode(Mode),
        .D(D),
        .Q(Q_sb),
        .rco(rco_sb),
        .Q_estr(Q_estr),
        .rco_estr(rco_estr)
    );

    contador_synt estructural(
        .clk(clk), 
        .en(en),
        .Mode(Mode),
        .D(D),
        .Q(Q_estr),
        .rco(rco_estr)
    );

endmodule