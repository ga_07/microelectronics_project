`timescale 1ns / 1ps

module scoreboard(
    input clk,
    input en,
    input [1:0] Mode,
    input [3:0] D,
    output reg [3:0] Q_sb,
    output reg rco_sb
);

    always @(posedge clk) begin
        if (en == 1 && Mode == 2'b00) begin
            Q_sb <= Q_sb+1;
            if (Q_sb == 4'b1111) begin
                rco_sb <= 1;
            end
            else begin
                rco_sb <= 0;
            end
        end
        if(en == 1 && Mode == 2'b01) begin
            Q_sb <= Q_sb-1;
            if (Q_sb == 4'b1111) begin
                rco_sb <= 1;
            end
            else begin
                rco_sb <= 0;
            end
        end
        if(en == 1 && Mode == 2'b10) begin
            Q_sb <= Q_sb-3;
            if ((Q_sb == 4'b1111) | (Q_sb == 4'b0001) | (Q_sb == 4'b0000)) begin
                rco_sb <= 1;
            end
            else begin
                rco_sb <= 0;
            end
        end
        if(en == 1 && Mode == 2'b11) begin
            Q_sb <= D;
            rco_sb <= 1;
        end
        if(!en) begin
            Q_sb <= D;
            rco_sb = 1;
        end
    end

endmodule