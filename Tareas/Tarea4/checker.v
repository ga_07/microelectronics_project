task checker;

input integer iteration;

repeat (iteration) @ (posedge clk) begin
	if ({sb.Q_sb,sb.rco_sb} == {Q, rco}) begin
          $fdisplay(log, "PASS");
        end
      else begin
            $fdisplay(log, "Time=%.0f Error Behavioral: Q=%b, rco=%b, scoreboard: Q=%b, RCO=%b", $time, Q, rco, sb.Q_sb, sb.rco_sb);
      end
      if ({Q, rco} == {Q_estr, rco_estr}) begin
          $fdisplay(log, "PASS");
        end
      else begin
            $fdisplay(log, "Time=%.0f Error Behavioral: Q=%b, rco=%b, estructural: Q=%b, RCO=%b", $time, Q, rco, Q_estr, rco_estr);
      end
end
endtask