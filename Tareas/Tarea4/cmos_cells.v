`timescale 1ns / 1ps

module NOT(A, Y);
    specify
        specparam tpdh = 4.2; //74AC11004 HEX INVERTER, typical value at Vcc = 5V +- 0.5V
        specparam tpdl = 3.8; //74AC11004 HEX INVERTER, typical value at Vcc = 5V +- 0.5V
        (A *> Y) = (tpdl, tpdh);
    endspecify

    input A;
    output Y;

    assign Y = ~A;
endmodule

module NAND(A, B, Y);
    specify
        specparam tpdh = 9; //SN74HCS30 Single8-InputNANDGate with Schmitt-TriggerInputs, typical value at Vcc = 4.5V
        specparam tpdl = 9; //SN74HCS30 Single8-InputNANDGate with Schmitt-TriggerInputs, typical value at Vcc = 4.5V
        (A *> Y) = (tpdl, tpdh);
        (B *> Y) = (tpdl, tpdh);
    endspecify

    input A, B;
    output Y;

    assign Y = ~(A&B);
endmodule

module NOR(A, B, Y);
    specify
        specparam tpdh = 7; //SN74HCS02Quadruple2-InputNOR Gateswith Schmitt-TriggerInputs, typical value at Vcc = 4.5V
        specparam tpdl = 7; //SN74HCS02Quadruple2-InputNOR Gateswith Schmitt-TriggerInputs, typical value at Vcc = 4.5V
        (A *> Y) = (tpdl, tpdh);
        (B *> Y) = (tpdl, tpdh);
    endspecify

    input A, B;
    output Y;

    assign Y = ~(A|B);
endmodule

module DFF(C, D, Q);
    specify
        specparam tpdh = 20; //SN74HC74-EP DUALD-TYPEPOSITIVEEDGETRIGGEREDFLIP-FLOP WITHCLEARANDPRESET, typical value at Vcc = 4.5V 
        specparam tpdl = 20; //SN74HC74-EP DUALD-TYPEPOSITIVEEDGETRIGGEREDFLIP-FLOP WITHCLEARANDPRESET, typical value at Vcc = 4.5V
        specparam tsetup = 20; //SN74HC74-EP DUALD-TYPEPOSITIVEEDGETRIGGEREDFLIP-FLOP WITHCLEARANDPRESET, typical value at Vcc = 4.5V
        specparam thold = 0; //SN74HC74-EP DUALD-TYPEPOSITIVEEDGETRIGGEREDFLIP-FLOP WITHCLEARANDPRESET, typical value at Vcc = 4.5V
        (C => Q) = (tpdl, tpdh);
        $setuphold(posedge C, D, tsetup, thold);
    endspecify

    input C, D;
    output reg Q;
    
    always @(posedge C)begin
        Q <= D;
    end
endmodule 