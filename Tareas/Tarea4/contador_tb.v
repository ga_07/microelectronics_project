`timescale 1ns / 1ps
`include "scoreboard.v"

module contador_tb #(
    parameter ITERATIONS = 15
)(
    output reg clk, en,
    output reg [1:0] Mode,
    output reg [3:0] D,
    input [3:0] Q, Q_estr,
    input rco, rco_estr
);

    `include "driver.v"
    `include "checker.v"
    integer log;
    wire rco_sb;
    wire [3:0] Q_sb;

    initial begin

        $dumpfile("counters.vcd");
        $dumpvars(0);

        log = $fopen("tb.log");
        $fdisplay(log, "time=%5d, Simulation Start", $time);
        $fdisplay(log, "time=%5d, Starting Reset", $time);

        driver_init(ITERATIONS);

        $fdisplay(log, "time=%5d, Reset Completed", $time);
        $fdisplay(log, "time=%5d, Starting Test", $time);

        fork
          driver_request(ITERATIONS);  
          checker(ITERATIONS);
        join

        $fdisplay(log, "time=%5d, Test Completed", $time);
        $fdisplay(log, "time=%5d, Simulation Completed", $time);
        $fclose(log);
        #200 $finish;

    end

    initial clk <= 0;
    always #200 clk <= ~clk;

    scoreboard sb(
        .clk(clk),
        .en(en),
        .Mode(Mode),
        .D(D),
        .Q_sb(Q_sb),
        .rco_sb(rco_sb)
    );

endmodule