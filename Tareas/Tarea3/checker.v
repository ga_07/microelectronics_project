task checker;

input integer iteration;

repeat (iteration) @ (posedge clk) begin
	if ({sb.Q,sb.RCO} == {Q_A, rco_A}) begin
          $fdisplay(log, "PASS");
        end
    else begin
          $fdisplay(log, "Time=%.0f Error duvA: Q_A=%b, rco_A=%b, scoreboard: Q=%b, RCO=%b", $time, Q_A, rco_A, sb.Q, sb.RCO);
    end
    if ({sb.Q,sb.RCO} == {Q_B, rco_B}) begin
          $fdisplay(log, "PASS");
        end
    else begin
          $fdisplay(log, "Time=%.0f Error duvB: Q_B=%b, rco_B=%b, scoreboard: Q=%b, RCO=%b", $time, Q_B, rco_B, sb.Q, sb.RCO);
    end
    if ({sb.Q,sb.RCO} == {Q_C, rco_C}) begin
          $fdisplay(log, "PASS");
    end        
    else begin
      $fdisplay(log, "Time=%.0f Error duvC: Q_C=%b, rco_C=%b, scoreboard: Q=%b, RCO=%b", $time, Q_C, rco_C, sb.Q, sb.RCO);
    end
end
endtask