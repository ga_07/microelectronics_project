module scoreboard(
    input clk,
    input en,
    input [1:0] Mode,
    input [3:0] D,
    output reg [3:0] Q,
    output reg RCO
);

    initial begin
        Q <= 0;
        RCO <= 0;
    end

    always @(posedge clk) begin
        if (en == 1 && Mode == 2'b00) begin
            Q <= Q+1;
            if (Q == 4'b1111) begin
                RCO <= 1;
            end
            else begin
                RCO <= 0;
            end
        end
        if(en == 1 && Mode == 2'b01) begin
            Q <= Q-1;
            if (Q == 4'b1111) begin
                RCO <= 1;
            end
            else begin
                RCO <= 0;
            end
        end
        if(en == 1 && Mode == 2'b10) begin
            Q <= Q-3;
            if ((Q == 4'b1111) | (Q == 4'b0001) | (Q == 4'b0000)) begin
                RCO <= 1;
            end
            else begin
                RCO <= 0;
            end
        end
        if(en == 1 && Mode == 2'b11) begin
            Q <= D;
            if (Q == 4'b1111) begin
                RCO <= 1;
            end
            else begin
                RCO <= 0;
            end
        end
    end

endmodule