# Tarea3 Microelectrónica

* Gabriel Gutiérrez Arguedas B63215
________________________________________________
________________________________________________

# Instrucciones de uso:
A continuación se muestran los comandos que debe ejecutar según la operación que desee realizar. 

#### Compilar el programa
    $ make
    
#### Motor del tiempo de ejecución
    $ make vvp

#### Visualizar las formas de onda
    $ make gtk