`include "scoreboard.v"
`include "contadorA.v"
`include "contadorB.v"
`include "contadorC.v"

module counter_tb #(
    parameter ITERATIONS = 15
)(
    output reg clk,
    output reg en,
    output reg [1:0] Mode,
    output reg [3:0] D,
    input [3:0] Q,
    input RCO
);

    `include "driver.v"
    `include "checker.v"
    integer log;

    wire rco_A, rco_B, rco_C;
    wire [3:0] Q_A, Q_B, Q_C;

    initial begin

        $dumpfile("counters.vcd");
        $dumpvars(0);

        log = $fopen("tb.log");
        $fdisplay(log, "time=%5d, Simulation Start", $time);
        $fdisplay(log, "time=%5d, Starting Reset", $time);

        driver_init(ITERATIONS);

        $fdisplay(log, "time=%5d, Reset Completed", $time);
        $fdisplay(log, "time=%5d, Starting Test", $time);

        fork
          driver_request(ITERATIONS);  
          checker(ITERATIONS);
        join

        $fdisplay(log, "time=%5d, Test Completed", $time);
        $fdisplay(log, "time=%5d, Simulation Completed", $time);
        $fclose(log);
        #200 $finish;

    end

    initial clk <= 0;
    always #5 clk <= ~clk;

    scoreboard sb(
        .clk(clk),
        .en(en),
        .Mode(Mode),
        .D(D),
        .Q(Q),
        .RCO(RCO)
    );

    countA duvA(
        .enable(en),
        .clk(clk),
        .modo(Mode),
        .D(D),
        .rco_A(rco_A),
        .Q_A(Q_A)
    );

    countB duvB(
        .enable(en),
        .clk(clk),
        .modo(Mode),
        .D(D),
        .rco_B(rco_B),
        .Q_B(Q_B)
    );

    countC duvC(
        .enable(en),
        .clk(clk),
        .modo(Mode),
        .D(D),
        .rco_C(rco_C),
        .Q_C(Q_C)
    );

endmodule
