`timescale 1ns / 1ps

module scoreboard(
    input clk,
    input en,
    input [2:0] Mode,
    input [31:0] D,
    output reg [31:0] Q_sb,
    output reg rco_sb
);

    always @(posedge clk) begin
        if (en) begin
            if (Mode == 3'b000) begin
                Q_sb <= Q_sb+1;
                if (Q_sb == 32'hFFFFFFFF) begin
                    rco_sb <= 1;
                end
                else begin
                    rco_sb <= 0;
                end
            end
            else if(Mode == 3'b001) begin
                Q_sb <= Q_sb-1;
                if (Q_sb == 32'h00000000) begin
                    rco_sb <= 1;
                end
                else begin
                    rco_sb <= 0;
                end
            end
            else if(Mode == 3'b010) begin
                Q_sb <= Q_sb-3;
                if ((Q_sb == 32'h00000002) | (Q_sb == 32'h00000001) | (Q_sb == 32'h00000000)) begin
                    rco_sb <= 1;
                end
                else begin
                    rco_sb <= 0;
                end
            end
            else if(Mode == 3'b011) begin
                Q_sb <= D;
                rco_sb <= 0;
            end
            else if(Mode == 3'b100) begin
                Q_sb <= 0;
                rco_sb <= 0;
            end
            else begin
                Q_sb <= 0;
                rco_sb <= 0;
            end
        end   
    end

    always @(negedge clk)begin
        rco_sb <= 0;
    end

endmodule