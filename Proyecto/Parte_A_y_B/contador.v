`timescale 1ns / 1ps

module contador #(
    parameter S0 = 3'b000, 
    parameter S1 = 3'b001, 
    parameter S2 = 3'b010, 
    parameter S3 = 3'b011, 
    parameter S4 = 3'b100
)(
    input clk, en,
    input [2:0] Mode,
    input [3:0] D,
    output reg [3:0] Q,
    output reg rco
); 

    always @(posedge clk)begin
        if (en) begin
            case(Mode)
                S0: begin
                    Q <= Q+1;
                    if (Q == 4'hF) begin
                        rco <= 1;
                    end else begin
                        rco <= 0;
                    end
                end
                S1: begin
                    Q <= Q-1;
                    if (Q == 4'h0) begin
                        rco <= 1;
                    end else begin
                        rco <= 0;
                    end
                end
                S2: begin
                    Q <= Q-3;
                    if (Q == 4'h2 | Q == 4'h0 | Q == 4'h1) begin
                        rco <= 1;
                    end else begin
                        rco <= 0;
                    end
                end
                S3: begin
                    Q <= D;
                    rco <= 0;
                end
                S4: begin
                    Q <= 0;
                    rco <= 0;
                end
                default: begin
                    Q <= 0;
                    rco <= 0;
                end
            endcase
        end
    end

    always @(negedge clk)begin
        rco <= 0;
    end

endmodule