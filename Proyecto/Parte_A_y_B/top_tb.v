`timescale 1ns / 1ps
`include "contador_tb.v"
`include "counter_32bits.v"

module top_tb;

    wire clk, en, rco_sb, RCO, rco_4; 
    wire [2:0] Mode;
    wire [31:0] D, Q, Q_sb, Q_32; 
    wire [3:0] D_4, Q_4;

    contador contador_4(
        .clk(clk), 
        .en(en),
        .Mode(Mode),
        .D(D_4),
        .Q(Q_4),
        .rco(rco_4)
    );

    counter_32bits contador_32(
        /*AUTOINST*/
			      // Outputs
			      .RCO		(RCO),
			      .Q_32		(Q_32),
			      // Inputs
			      .clk		(clk),
			      .en0		(en),
			      .Mode		(Mode),
			      .D		(D));

    contador_tb testbench(
        .clk(clk), 
        .en(en),
        .Mode(Mode),
        .D(D),
        .D_4(D_4),
        .Q(Q_sb),
        .Q_4(Q_4),
        .rco(RCO),
        .rco_4(rco_4)
    );

endmodule
