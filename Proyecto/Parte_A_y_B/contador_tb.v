`timescale 1ns / 1ps
`include "scoreboard.v"
`include "scoreboard_4.v"

module contador_tb #(
    parameter ITERATIONS = 550
)(
    output reg clk, en,
    output reg [2:0] Mode,
    output reg [31:0] D,
    output reg [3:0] D_4,
    input [31:0] Q,
    input [3:0] Q_4,
    input rco, rco_4
);

    `include "driver.v"
    `include "checker.v"
    integer log;
    wire rco_sb, rco_sb_4;
    wire [31:0] Q_sb;
    wire [3:0] Q_sb_4;

    initial begin

        $dumpfile("counters.vcd");
        $dumpvars(0);

        log = $fopen("tb.log");
        $fdisplay(log, "time=%5d, Simulation Start", $time);
        $fdisplay(log, "time=%5d, Starting Reset", $time);

        driver_init(ITERATIONS);

        $fdisplay(log, "time=%5d, Reset Completed", $time);
        $fdisplay(log, "time=%5d, Starting Test", $time);

        fork
          driver_request(ITERATIONS);  
          checker(ITERATIONS);
        join

        $fdisplay(log, "time=%5d, Test Completed", $time);
        $fdisplay(log, "time=%5d, Simulation Completed", $time);
        $fclose(log);
        #200 $finish;

    end

    initial clk <= 0;
    always #200 clk <= ~clk;

    scoreboard sb_32(
        .clk(clk),
        .en(en),
        .Mode(Mode),
        .D(D),
        .Q_sb(Q_sb),
        .rco_sb(rco_sb)
    );

    scoreboard_4 sb_4(
        .clk(clk),
        .en(en),
        .Mode_4(Mode),
        .D_4(D_4),
        .Q_sb_4(Q_sb_4),
        .rco_sb_4(rco_sb_4)
    );

endmodule