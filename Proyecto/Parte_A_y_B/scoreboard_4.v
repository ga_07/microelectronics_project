`timescale 1ns / 1ps

module scoreboard_4(
    input clk,
    input en,
    input [2:0] Mode_4,
    input [3:0] D_4,
    output reg [3:0] Q_sb_4,
    output reg rco_sb_4
);

    always @(posedge clk) begin
        if (en) begin

            if (Mode_4 == 3'b000) begin
                Q_sb_4 <= Q_sb_4+1;
                if (Q_sb_4 == 4'hF) begin
                    rco_sb_4 <= 1;
                end
                else begin
                    rco_sb_4 <= 0;
                end
            end
            else if(Mode_4 == 3'b001) begin
                Q_sb_4 <= Q_sb_4-1;
                if (Q_sb_4 == 4'h0) begin
                    rco_sb_4 <= 1;
                end
                else begin
                    rco_sb_4 <= 0;
                end
            end
            else if(Mode_4 == 3'b010) begin
                Q_sb_4 <= Q_sb_4-3;
                if ((Q_sb_4 == 32'h2) | (Q_sb_4 == 4'h1) | (Q_sb_4 == 32'h0)) begin
                    rco_sb_4 <= 1;
                end
                else begin
                    rco_sb_4 <= 0;
                end
            end
            else if(Mode_4 == 3'b011) begin
                Q_sb_4 <= D_4;
                rco_sb_4 <= 0;
            end
            else if(Mode_4 == 3'b100) begin
                Q_sb_4 <= 0;
                rco_sb_4 <= 0;
            end
            else begin
                Q_sb_4 <= 0;
                rco_sb_4 <= 0;
            end
        end   
    end

    always @(negedge clk)begin
        rco_sb_4 <= 0;
    end

endmodule