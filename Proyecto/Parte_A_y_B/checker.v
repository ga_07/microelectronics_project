task checker;

input integer iteration;

repeat (iteration) @ (posedge clk) begin
	if ({sb_32.Q_sb,sb_32.rco_sb} == {Q, rco}) begin
          $fdisplay(log, "PASS");
        end
      else begin
            $fdisplay(log, "Time=%.0f Error Behavioral: Q_32=%b, rco_32=%b, scoreboard: Q_32=%b, RCO_32=%b", $time, Q, rco, sb_32.Q_sb, sb_32.rco_sb);
      end
      if ({sb_4.Q_sb_4,sb_4.rco_sb_4} == {Q_4, rco_4}) begin
          $fdisplay(log, "PASS");
        end
      else begin
            $fdisplay(log, "Time=%.0f Error Behavioral: Q_4=%b, rco_4=%b, scoreboard: Q_4=%b, RCO_4=%b", $time, Q_4, rco_4, sb_4.Q_sb_4, sb_4.rco_sb_4);
      end
end
endtask