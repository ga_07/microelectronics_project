task driver_init;
input integer iteration;
  begin
    @(negedge clk)
      en = 0;
      D = 32'h00000000;
      Mode = 3'b100;
      D_4 = 4'h0;
    @(negedge clk)
    @(negedge clk)
      en = 0;
      D = 32'h00001010;
      Mode = 3'b100;
      D_4 = 4'hA;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00001011;
      Mode = 3'b100;
      D_4 = 4'hB;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00000111;
      Mode = 3'b100;
      D_4 = 4'h7;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00001001;
      Mode = 3'b100;
      D_4 = 4'h9;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00000110;
      Mode = 3'b100;
      D_4 = 4'h6;
    @(negedge clk)
      en = 1;
      D = 32'h00000000;
      Mode = 3'b100;
      D_4 = 4'h0;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00001010;
      Mode = 3'b100;
      D_4 = 4'hA;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00001011;
      Mode = 3'b100;
      D_4 = 4'hC;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00000111;
      Mode = 3'b100;
      D_4 = 4'h7;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00001001;
      Mode = 3'b100;
      D_4 = 4'h9;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00000110;
      Mode = 3'b100;
      D_4 = 4'h6;
    @(negedge clk)
      en = 1;
      D = 32'h00000100;
      Mode = 3'b011;
      D_4 = 4'h4;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00001111;
      Mode = 3'b011;
      D_4 = 4'hF;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00000001;
      Mode = 3'b011;
      D_4 = 4'h1;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00001011;
      Mode = 3'b011;
      D_4 = 4'hD;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00001000;
      Mode = 3'b011;
      D_4 = 4'h8;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00000101;
      Mode = 3'b011;
      D_4 = 4'h5;
    @(negedge clk)
    @(negedge clk)
      en = 1;
      D = 32'h00000000;
      Mode = 3'b011;
      D_4 = 4'h0;
    @(negedge clk)
    @(negedge clk)
    repeat (iteration) begin
        @(negedge clk) begin
            en = 1;
            Mode = 3'b000; 
        end
    end
    @(negedge clk)
    repeat (iteration) begin
        @(negedge clk) begin
            en = 1;
            Mode = 3'b001; 
        end
    end
    @(negedge clk)
    repeat (iteration) begin
        @(negedge clk) begin
            en = 1;
            Mode = 3'b010; 
        end
    end      
    @(negedge clk)
    repeat (iteration) begin
        @(negedge clk) begin
            en = 1;
            Mode = 3'b001; 
        end
    end 
    @(negedge clk)
    repeat (iteration) begin
        @(negedge clk) begin
            en = 0;
            Mode = 3'b001; 
        end
    end
    @(negedge clk)
    en = 1;
    Mode = 3'b011;
    D = 32'hFFFFFFFF;
    @(negedge clk)
    Mode = 3'b000;
    @(negedge clk)
    Mode = 3'b001;
    @(negedge clk)
    Mode = 3'b011;
    D = 32'h00000000;
    @(negedge clk)
    Mode = 3'b001;
    @(negedge clk)
    Mode = 3'b000;
    @(negedge clk)
    Mode = 3'b011;
    D = 32'h00000000;
    @(negedge clk)
    Mode = 3'b010;
    @(negedge clk)
    Mode = 3'b011;
    D = 32'h00000001;
    @(negedge clk)
    Mode = 3'b010;
    @(negedge clk)
    Mode = 3'b011;
    D = 32'h00000002;
    @(negedge clk)
    Mode = 3'b010;
  end
endtask

task driver_request;
input integer iteration;
  repeat (iteration) begin  
    @(negedge clk) begin
        en = 1;
        Mode = $random;
        D = $random;
        D_4 = $random;
    end
  end
endtask