`include "contador.v"

module counter_32bits(
    input clk, en0,
    input [2:0] Mode,
    input [31:0] D,
    output RCO,
    output [31:0] Q_32
);

    wire rco0, rco1, rco2, rco3, rco4, rco5, rco6, rco7; 
    wire en2, en3, en4, en5, en6, en7;
    wire [3:0] Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7;
	wire clk1, clk2, clk3, clk4, clk5, clk6, clk7;
	wire [2:0] Mode0, Mode1, Mode2, Mode3, Mode4, Mode5, Mode6, Mode7;
    
    assign en1 = en0;
    assign en2 = en0;	
    assign en3 = en0;
    assign en4 = en0;
    assign en5 = en0;
	assign en6 = en0;
	assign en7 = en0;

	assign Mode0 = (Mode == 3'b000 || Mode == 3'b001 || Mode == 3'b010 || Mode == 3'b011 || Mode == 3'b100) ? Mode : 3'b100;

	assign clk1 = (Mode0 == 3'b100 || Mode0 == 3'b011) ? clk  : rco0;
	assign clk2 = (Mode0 == 3'b100 || Mode0 == 3'b011) ? clk  : rco1;
	assign clk3 = (Mode0 == 3'b100 || Mode0 == 3'b011) ? clk  : rco2;
	assign clk4 = (Mode0 == 3'b100 || Mode0 == 3'b011) ? clk  : rco3;
	assign clk5 = (Mode0 == 3'b100 || Mode0 == 3'b011) ? clk  : rco4;
	assign clk6 = (Mode0 == 3'b100 || Mode0 == 3'b011) ? clk  : rco5;
	assign clk7 = (Mode0 == 3'b100 || Mode0 == 3'b011) ? clk  : rco6;

	assign Mode1 = (Mode0 == 3'b010) ? 3'b001 : Mode0;
	assign Mode2 = (Mode0 == 3'b010) ? 3'b001 : Mode0;
	assign Mode3 = (Mode0 == 3'b010) ? 3'b001 : Mode0;
	assign Mode4 = (Mode0 == 3'b010) ? 3'b001 : Mode0;
	assign Mode5 = (Mode0 == 3'b010) ? 3'b001 : Mode0;
	assign Mode6 = (Mode0 == 3'b010) ? 3'b001 : Mode0;
	assign Mode7 = (Mode0 == 3'b010) ? 3'b001 : Mode0;

	assign RCO = (rco0 && rco1 && rco2 && rco3 && rco4 && rco5 && rco6 && rco7);

    assign Q_32[3:0] = Q0;
    assign Q_32[7:4] = Q1;
    assign Q_32[11:8] = Q2;
    assign Q_32[15:12] = Q3;
    assign Q_32[19:16] = Q4;
    assign Q_32[23:20] = Q5;
    assign Q_32[27:24] = Q6;
    assign Q_32[31:28] = Q7;

    contador contador0(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q0),
		       .rco		(rco0),
		       // Inputs
		       .clk		(clk),
		       .en		(en0),
		       .Mode		(Mode0),
		       .D		(D[3:0]));

    contador contador1(
       /*AUTOINST*/
		       // Outputs
		       .Q		(Q1),
		       .rco		(rco1),
		       // Inputs
		       .clk		(clk1),
		       .en		(en1),
		       .Mode		(Mode1),
		       .D		(D[7:4]));

    contador contador2(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q2),
		       .rco		(rco2),
		       // Inputs
		       .clk		(clk2),
		       .en		(en2),
		       .Mode		(Mode2),
		       .D		(D[11:8]));

    contador contador3(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q3),
		       .rco		(rco3),
		       // Inputs
		       .clk		(clk3),
		       .en		(en3),
		       .Mode		(Mode3),
		       .D		(D[15:12]));

    contador contador4(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q4),
		       .rco		(rco4),
		       // Inputs
		       .clk		(clk4),
		       .en		(en4),
		       .Mode		(Mode4),
		       .D		(D[19:16]));

    contador contador5(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q5),
		       .rco		(rco5),
		       // Inputs
		       .clk		(clk5),
		       .en		(en5),
		       .Mode		(Mode5),
		       .D		(D[23:20]));
    
    contador contador6(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q6),
		       .rco		(rco6),
		       // Inputs
		       .clk		(clk6),
		       .en		(en6),
		       .Mode		(Mode6),
		       .D		(D[27:24]));

    contador contador7(
        /*AUTOINST*/
		       // Outputs
		       .Q		(Q7),
		       .rco		(rco7),
		       // Inputs
		       .clk		(clk7),
		       .en		(en7),
		       .Mode		(Mode7),
		       .D		(D[31:28]));

endmodule
