#!/usr/bin/tcsh -f
#-------------------------------------------
# qflow variables for project /home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu050library/4bits
#-------------------------------------------

set projectpath=/home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu050library/4bits
set techdir=/usr/share/qflow/tech/osu050
set sourcedir=/home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu050library/4bits/source
set synthdir=/home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu050library/4bits/synthesis
set layoutdir=/home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu050library/4bits/layout
set techname=osu050
set scriptdir=/usr/lib/qflow/scripts
set bindir=/usr/lib/qflow/bin
set synthlog=/home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu050library/4bits/synth.log
#-------------------------------------------

