*SPICE netlist created from BLIF module contador by blif2BSpice
.include /usr/share/qflow/tech/osu050/osu050_stdcells.sp
.subckt contador vdd gnd clk en Mode<0> Mode<1> Mode<2> D<0> D<1> D<2> D<3> Q<0> Q<1> Q<2> Q<3> rco 
XAOI21X1_1 gnd vdd _40_ _42_ _43_ _18_ AOI21X1
XNAND3X1_1 _54_<3> vdd gnd _54_<2> _11_ _44_ NAND3X1
XNAND2X1_1 vdd _45_ gnd _41_ _26_ NAND2X1
XNAND3X1_2 _45_ vdd gnd _23_ _44_ _46_ NAND3X1
XNAND2X1_2 vdd _47_ gnd D<3> _6_ NAND2X1
XOAI21X1_1 gnd vdd _54_<2> _11_ _48_ _41_ OAI21X1
XNAND2X1_3 vdd _49_ gnd _54_<3> _24_ NAND2X1
XNAND3X1_3 _34_ vdd gnd _49_ _48_ _50_ NAND3X1
XNAND3X1_4 _47_ vdd gnd _46_ _50_ _51_ NAND3X1
XOAI21X1_2 gnd vdd _43_ _51_ _52_ _3_ OAI21X1
XNAND2X1_4 vdd _53_ gnd _54_<3> _2_ NAND2X1
XNAND2X1_5 vdd _0_<3> gnd _53_ _52_ NAND2X1
XBUFX2_1 vdd gnd _54_<0> Q<0> BUFX2
XBUFX2_2 vdd gnd _54_<1> Q<1> BUFX2
XBUFX2_3 vdd gnd _54_<2> Q<2> BUFX2
XBUFX2_4 vdd gnd _54_<3> Q<3> BUFX2
XBUFX2_5 vdd gnd gnd rco BUFX2
XDFFPOSX1_1 vdd _0_<0> gnd _54_<0> clk DFFPOSX1
XDFFPOSX1_2 vdd _0_<1> gnd _54_<1> clk DFFPOSX1
XDFFPOSX1_3 vdd _0_<2> gnd _54_<2> clk DFFPOSX1
XDFFPOSX1_4 vdd _0_<3> gnd _54_<3> clk DFFPOSX1
XINVX1_1 _54_<0> _1_ vdd gnd INVX1
XINVX1_2 en _2_ vdd gnd INVX1
XNOR2X1_1 vdd _2_ gnd _3_ Mode<2> NOR2X1
XINVX1_3 _3_ _4_ vdd gnd INVX1
XNAND2X1_6 vdd _5_ gnd Mode<1> Mode<0> NAND2X1
XNOR2X1_2 vdd _5_ gnd _6_ Mode<2> NOR2X1
XNOR2X1_3 vdd Mode<2> gnd _7_ _54_<0> NOR2X1
XAOI22X1_1 gnd vdd _6_ D<0> _8_ _5_ _7_ AOI22X1
XOAI22X1_1 gnd vdd _8_ _4_ _1_ en _0_<0> OAI22X1
XINVX1_4 _54_<1> _9_ vdd gnd INVX1
XNOR2X1_4 vdd Mode<0> gnd _10_ Mode<2> NOR2X1
XAND2X2_1 vdd gnd _54_<0> _54_<1> _11_ AND2X2
XNOR2X1_5 vdd _54_<1> gnd _12_ _54_<0> NOR2X1
XNOR2X1_6 vdd _11_ gnd _13_ _12_ NOR2X1
XAND2X2_2 vdd gnd _13_ _10_ _14_ AND2X2
XINVX1_5 Mode<1> _15_ vdd gnd INVX1
XINVX1_6 Mode<0> _16_ vdd gnd INVX1
XNOR2X1_7 vdd _16_ gnd _17_ Mode<2> NOR2X1
XNAND2X1_7 vdd _18_ gnd _15_ _17_ NAND2X1
XNAND2X1_8 vdd _19_ gnd D<1> _6_ NAND2X1
XOAI21X1_3 gnd vdd _13_ _18_ _20_ _19_ OAI21X1
XOAI21X1_4 gnd vdd _14_ _20_ _21_ _3_ OAI21X1
XOAI21X1_5 gnd vdd en _9_ _0_<1> _21_ OAI21X1
XINVX1_7 _54_<2> _22_ vdd gnd INVX1
XNOR3X1_1 vdd gnd Mode<2> Mode<0> Mode<1> _23_ NOR3X1
XAOI21X1_2 gnd vdd _54_<0> _54_<1> _24_ _54_<2> AOI21X1
XINVX1_8 _24_ _25_ vdd gnd INVX1
XNAND3X1_5 _54_<1> vdd gnd _54_<0> _54_<2> _26_ NAND3X1
XNAND3X1_6 _23_ vdd gnd _26_ _25_ _27_ NAND3X1
XNAND2X1_9 vdd _28_ gnd D<2> _6_ NAND2X1
XNAND2X1_10 vdd _29_ gnd _28_ _27_ NAND2X1
XNOR3X1_2 vdd gnd Mode<2> _16_ Mode<1> _30_ NOR3X1
XAOI21X1_3 gnd vdd _1_ _9_ _31_ _22_ AOI21X1
XNOR3X1_3 vdd gnd _54_<1> _54_<2> _54_<0> _32_ NOR3X1
XOAI21X1_6 gnd vdd _31_ _32_ _33_ _30_ OAI21X1
XNOR3X1_4 vdd gnd Mode<0> _15_ Mode<2> _34_ NOR3X1
XINVX1_9 _26_ _35_ vdd gnd INVX1
XOAI21X1_7 gnd vdd _24_ _35_ _36_ _34_ OAI21X1
XNAND2X1_11 vdd _37_ gnd _33_ _36_ NAND2X1
XOAI21X1_8 gnd vdd _29_ _37_ _38_ _3_ OAI21X1
XOAI21X1_9 gnd vdd en _22_ _0_<2> _38_ OAI21X1
XNAND2X1_12 vdd _39_ gnd _1_ _9_ NAND2X1
XOAI21X1_10 gnd vdd _54_<2> _39_ _40_ _54_<3> OAI21X1
XINVX1_10 _54_<3> _41_ vdd gnd INVX1
XNAND2X1_13 vdd _42_ gnd _41_ _32_ NAND2X1
.ends contador
 