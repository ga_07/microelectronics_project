module contador ( gnd, vdd, clk, en, Mode, D, Q, rco);

input gnd, vdd;
input clk;
input en;
output rco;
input [2:0] Mode;
input [3:0] D;
output [3:0] Q;

	AOI21X1 AOI21X1_1 ( .gnd(gnd), .vdd(vdd), .A(_40_), .B(_42_), .C(_18_), .Y(_43_) );
	NAND3X1 NAND3X1_1 ( .gnd(gnd), .vdd(vdd), .A(_54__2_), .B(_54__3_), .C(_11_), .Y(_44_) );
	NAND2X1 NAND2X1_1 ( .gnd(gnd), .vdd(vdd), .A(_41_), .B(_26_), .Y(_45_) );
	NAND3X1 NAND3X1_2 ( .gnd(gnd), .vdd(vdd), .A(_23_), .B(_45_), .C(_44_), .Y(_46_) );
	NAND2X1 NAND2X1_2 ( .gnd(gnd), .vdd(vdd), .A(D[3]), .B(_6_), .Y(_47_) );
	OAI21X1 OAI21X1_1 ( .gnd(gnd), .vdd(vdd), .A(_54__2_), .B(_11_), .C(_41_), .Y(_48_) );
	NAND2X1 NAND2X1_3 ( .gnd(gnd), .vdd(vdd), .A(_54__3_), .B(_24_), .Y(_49_) );
	NAND3X1 NAND3X1_3 ( .gnd(gnd), .vdd(vdd), .A(_49_), .B(_34_), .C(_48_), .Y(_50_) );
	NAND3X1 NAND3X1_4 ( .gnd(gnd), .vdd(vdd), .A(_46_), .B(_47_), .C(_50_), .Y(_51_) );
	OAI21X1 OAI21X1_2 ( .gnd(gnd), .vdd(vdd), .A(_43_), .B(_51_), .C(_3_), .Y(_52_) );
	NAND2X1 NAND2X1_4 ( .gnd(gnd), .vdd(vdd), .A(_54__3_), .B(_2_), .Y(_53_) );
	NAND2X1 NAND2X1_5 ( .gnd(gnd), .vdd(vdd), .A(_53_), .B(_52_), .Y(_0__3_) );
	BUFX2 BUFX2_1 ( .gnd(gnd), .vdd(vdd), .A(_54__0_), .Y(Q[0]) );
	BUFX2 BUFX2_2 ( .gnd(gnd), .vdd(vdd), .A(_54__1_), .Y(Q[1]) );
	BUFX2 BUFX2_3 ( .gnd(gnd), .vdd(vdd), .A(_54__2_), .Y(Q[2]) );
	BUFX2 BUFX2_4 ( .gnd(gnd), .vdd(vdd), .A(_54__3_), .Y(Q[3]) );
	BUFX2 BUFX2_5 ( .gnd(gnd), .vdd(vdd), .A(gnd), .Y(rco) );
	DFFPOSX1 DFFPOSX1_1 ( .gnd(gnd), .vdd(vdd), .CLK(clk), .D(_0__0_), .Q(_54__0_) );
	DFFPOSX1 DFFPOSX1_2 ( .gnd(gnd), .vdd(vdd), .CLK(clk), .D(_0__1_), .Q(_54__1_) );
	DFFPOSX1 DFFPOSX1_3 ( .gnd(gnd), .vdd(vdd), .CLK(clk), .D(_0__2_), .Q(_54__2_) );
	DFFPOSX1 DFFPOSX1_4 ( .gnd(gnd), .vdd(vdd), .CLK(clk), .D(_0__3_), .Q(_54__3_) );
	INVX1 INVX1_1 ( .gnd(gnd), .vdd(vdd), .A(_54__0_), .Y(_1_) );
	INVX1 INVX1_2 ( .gnd(gnd), .vdd(vdd), .A(en), .Y(_2_) );
	NOR2X1 NOR2X1_1 ( .gnd(gnd), .vdd(vdd), .A(Mode[2]), .B(_2_), .Y(_3_) );
	INVX1 INVX1_3 ( .gnd(gnd), .vdd(vdd), .A(_3_), .Y(_4_) );
	NAND2X1 NAND2X1_6 ( .gnd(gnd), .vdd(vdd), .A(Mode[1]), .B(Mode[0]), .Y(_5_) );
	NOR2X1 NOR2X1_2 ( .gnd(gnd), .vdd(vdd), .A(Mode[2]), .B(_5_), .Y(_6_) );
	NOR2X1 NOR2X1_3 ( .gnd(gnd), .vdd(vdd), .A(_54__0_), .B(Mode[2]), .Y(_7_) );
	AOI22X1 AOI22X1_1 ( .gnd(gnd), .vdd(vdd), .A(_5_), .B(_7_), .C(_6_), .D(D[0]), .Y(_8_) );
	OAI22X1 OAI22X1_1 ( .gnd(gnd), .vdd(vdd), .A(_1_), .B(en), .C(_4_), .D(_8_), .Y(_0__0_) );
	INVX1 INVX1_4 ( .gnd(gnd), .vdd(vdd), .A(_54__1_), .Y(_9_) );
	NOR2X1 NOR2X1_4 ( .gnd(gnd), .vdd(vdd), .A(Mode[2]), .B(Mode[0]), .Y(_10_) );
	AND2X2 AND2X2_1 ( .gnd(gnd), .vdd(vdd), .A(_54__0_), .B(_54__1_), .Y(_11_) );
	NOR2X1 NOR2X1_5 ( .gnd(gnd), .vdd(vdd), .A(_54__0_), .B(_54__1_), .Y(_12_) );
	NOR2X1 NOR2X1_6 ( .gnd(gnd), .vdd(vdd), .A(_12_), .B(_11_), .Y(_13_) );
	AND2X2 AND2X2_2 ( .gnd(gnd), .vdd(vdd), .A(_13_), .B(_10_), .Y(_14_) );
	INVX1 INVX1_5 ( .gnd(gnd), .vdd(vdd), .A(Mode[1]), .Y(_15_) );
	INVX1 INVX1_6 ( .gnd(gnd), .vdd(vdd), .A(Mode[0]), .Y(_16_) );
	NOR2X1 NOR2X1_7 ( .gnd(gnd), .vdd(vdd), .A(Mode[2]), .B(_16_), .Y(_17_) );
	NAND2X1 NAND2X1_7 ( .gnd(gnd), .vdd(vdd), .A(_15_), .B(_17_), .Y(_18_) );
	NAND2X1 NAND2X1_8 ( .gnd(gnd), .vdd(vdd), .A(D[1]), .B(_6_), .Y(_19_) );
	OAI21X1 OAI21X1_3 ( .gnd(gnd), .vdd(vdd), .A(_13_), .B(_18_), .C(_19_), .Y(_20_) );
	OAI21X1 OAI21X1_4 ( .gnd(gnd), .vdd(vdd), .A(_14_), .B(_20_), .C(_3_), .Y(_21_) );
	OAI21X1 OAI21X1_5 ( .gnd(gnd), .vdd(vdd), .A(en), .B(_9_), .C(_21_), .Y(_0__1_) );
	INVX1 INVX1_7 ( .gnd(gnd), .vdd(vdd), .A(_54__2_), .Y(_22_) );
	NOR3X1 NOR3X1_1 ( .gnd(gnd), .vdd(vdd), .A(Mode[1]), .B(Mode[2]), .C(Mode[0]), .Y(_23_) );
	AOI21X1 AOI21X1_2 ( .gnd(gnd), .vdd(vdd), .A(_54__0_), .B(_54__1_), .C(_54__2_), .Y(_24_) );
	INVX1 INVX1_8 ( .gnd(gnd), .vdd(vdd), .A(_24_), .Y(_25_) );
	NAND3X1 NAND3X1_5 ( .gnd(gnd), .vdd(vdd), .A(_54__0_), .B(_54__1_), .C(_54__2_), .Y(_26_) );
	NAND3X1 NAND3X1_6 ( .gnd(gnd), .vdd(vdd), .A(_26_), .B(_23_), .C(_25_), .Y(_27_) );
	NAND2X1 NAND2X1_9 ( .gnd(gnd), .vdd(vdd), .A(D[2]), .B(_6_), .Y(_28_) );
	NAND2X1 NAND2X1_10 ( .gnd(gnd), .vdd(vdd), .A(_28_), .B(_27_), .Y(_29_) );
	NOR3X1 NOR3X1_2 ( .gnd(gnd), .vdd(vdd), .A(Mode[1]), .B(Mode[2]), .C(_16_), .Y(_30_) );
	AOI21X1 AOI21X1_3 ( .gnd(gnd), .vdd(vdd), .A(_1_), .B(_9_), .C(_22_), .Y(_31_) );
	NOR3X1 NOR3X1_3 ( .gnd(gnd), .vdd(vdd), .A(_54__0_), .B(_54__1_), .C(_54__2_), .Y(_32_) );
	OAI21X1 OAI21X1_6 ( .gnd(gnd), .vdd(vdd), .A(_31_), .B(_32_), .C(_30_), .Y(_33_) );
	NOR3X1 NOR3X1_4 ( .gnd(gnd), .vdd(vdd), .A(Mode[2]), .B(Mode[0]), .C(_15_), .Y(_34_) );
	INVX1 INVX1_9 ( .gnd(gnd), .vdd(vdd), .A(_26_), .Y(_35_) );
	OAI21X1 OAI21X1_7 ( .gnd(gnd), .vdd(vdd), .A(_24_), .B(_35_), .C(_34_), .Y(_36_) );
	NAND2X1 NAND2X1_11 ( .gnd(gnd), .vdd(vdd), .A(_33_), .B(_36_), .Y(_37_) );
	OAI21X1 OAI21X1_8 ( .gnd(gnd), .vdd(vdd), .A(_29_), .B(_37_), .C(_3_), .Y(_38_) );
	OAI21X1 OAI21X1_9 ( .gnd(gnd), .vdd(vdd), .A(en), .B(_22_), .C(_38_), .Y(_0__2_) );
	NAND2X1 NAND2X1_12 ( .gnd(gnd), .vdd(vdd), .A(_1_), .B(_9_), .Y(_39_) );
	OAI21X1 OAI21X1_10 ( .gnd(gnd), .vdd(vdd), .A(_54__2_), .B(_39_), .C(_54__3_), .Y(_40_) );
	INVX1 INVX1_10 ( .gnd(gnd), .vdd(vdd), .A(_54__3_), .Y(_41_) );
	NAND2X1 NAND2X1_13 ( .gnd(gnd), .vdd(vdd), .A(_41_), .B(_32_), .Y(_42_) );
endmodule
