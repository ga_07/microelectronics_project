#!/usr/bin/tcsh -f
#-------------------------------------------
# qflow variables for project /home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu035library/32bits
#-------------------------------------------

set projectpath=/home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu035library/32bits
set techdir=/usr/share/qflow/tech/osu035
set sourcedir=/home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu035library/32bits/source
set synthdir=/home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu035library/32bits/synthesis
set layoutdir=/home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu035library/32bits/layout
set techname=osu035
set scriptdir=/usr/lib/qflow/scripts
set bindir=/usr/lib/qflow/bin
set synthlog=/home/gabriel/Desktop/Microelectronica/microelectronics_project/Parte_C/osu035library/32bits/synth.log
#-------------------------------------------

